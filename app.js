const express = require('express'); 

const bodyParser = require('body-parser');

const db = require('./util/database');
const winston = require('winston');
const router = express.Router();

const users = require('./routes/users');
const admin = require('./routes/admin');
const log = require('./util/logging');






const app = express();
const PORT = 3000;
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)


    


app.use('/api/vi/user', users.routes);

app.use('/api/vi/admin', admin.routes);
//const URL = `http://localhost:${PORT}/`;


const server = app.listen(PORT);

log.info('Server Started on Port' + PORT);

const socket = require('socket.io')(server); // Establish a socket connection using http 
socket.on('connection',socket => {
  console.log('Client connected');
})
