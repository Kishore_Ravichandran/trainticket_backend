const util = require('util');
const connection = require('../util/database');

var mysql = require("mysql2");
const jwt = require('jsonwebtoken');
const generateUniqueId = require('generate-unique-id');
const logger = require('../util/logging');



exports.login = (req, res, next) => {
    logger.info('In Login');
    const email = req.body.email; 
    const password = req.body.password;
    var query = `Select * from users WHERE emailid=?`;
    query = mysql.format(query, email);
    connection.query( query, (err,result) => {
        if(err) {
            logger.error('Error while logging' + err);
            res.status(500).send('Server error!' + err);
        } 
        logger.info('Login Users length' + result.rows.length);
        var output = JSON.parse(JSON.stringify(result.rows));
        if(result.rows.length == 1) {
            var userDetails = {
                user_name : output[0].firstname,
                emailid : output[0].emailid,
                user_type : output[0].usertype
            }
            if (password === output[0].password) {
                                let token = jwt.sign(userDetails, process.env.SECRET_KEY, {
                                    expiresIn: 1440
                                });
                                 res.status(200).json({ token: token, expiresIn: 1440 });
                             } else {
                                 res.status(401).send('Password not valid!')
                             }
        } else {
            res.status(404).send('User Not Found');
        }
        
    })
    
};

exports.register = (req,res) => {
    logger.info('In register ');
    const email = req.body.emailid; 
    var query = `Select * from users WHERE emailid=?`;
    query = mysql.format(query, email);
    connection.query(query,(err,result) => {
        if(err) {
            logger.error('Error while registering users'+err);
            res.status(500).send('Server error!' + err);
        } 

       if(result.rows.length === 0) {
            var query = `INSERT into users (emailid, firstname, lastname,usertype,password) VALUES($1, $2, $3,$4,$5)`;
            var updateValues = [req.body.emailid,req.body.fname,req.body.lname,"U",req.body.password];
            connection.query(query,updateValues,(err,result) => {
                if(err) {
                    logger.error('Error while registering users'+err);
                    res.status(500).send('Server error!' + err);
                } else {
                    res.status(200).json({ user: 'User Created Successfully' });
                }
            })
       } else {
        res.status(409).send('User Already Found!' + err);
       }

    })


}

exports.searchTrain = (req,res) => {
    logger.info('In Search Train Panel')
    var query = 'SELECT * from traininfo WHERE fromdest=$1 AND todest = $2 AND date = $3';
    var updateValues = [req.body.from,req.body.to,req.body.doj];
    connection.query(query,updateValues,(err,result) => {
        if(err) {
            logger.error('Error while searching for train' + err);
            res.status(500).send('Server error!' + err);
        } else {
            var output = JSON.parse(JSON.stringify(result.rows))
            res.status(200).json(output);
        }
    })
}

exports.bookTicket = (req,res) => {
    const id = generateUniqueId({
        length: 8,
        useLetters: false
      });
    var query = 'INSERT into booking(trainid,pnrnumber,fromdest,todest,seats,doj,userid) VALUES ($1,$2,$3,$4,$5,$6,$7)';
    var updateValues = [req.body.trainid,id,req.body.fromdest,req.body.todest,req.body.seats.name,req.body.date,req.body.userid];
    connection.query(query,updateValues,(err,result)=> {
        if(err) {
            logger.error('Error while searching for train' + err);
            res.status(500).send('Server error!' + err);
        } 
        res.status(200).json({book : 'Booking Created'});
    })
}