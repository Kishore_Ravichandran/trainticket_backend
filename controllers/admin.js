const util = require('util');
const connection = require('../util/database');
var mysql = require("mysql2");
const logger = require('../util/logging');


exports.booking = (req,res) => {
    logger.info('In Booking List');
    var query = `SELECT * from booking`;
    connection.query(query,(err,result)=>{
        if(err) {
            logger.error('Error in Booking List' + err)
            res.status(500).send('Server error!' + err);
        } 
        res.status(200).json(result.rows);

    })
}

exports.trainList = (req,res)=>{
    logger.info('In Train List');
    var query = `SELECT * from traininfo`;
    connection.query(query,(err,result)=>{
        if(err) {
            logger.error('Error in Train List' + err);
            res.status(500).send('Server error!' + err);
        } 
        res.status(200).json(result.rows);
    })
}

exports.updateBooking = (req,res) => {
    logger.info('In Update Booking List');
    var bookingPnr = req.body.pnrnumber;
    var query = 'UPDATE booking SET trainid=($1),seats=($2),userid=($3) WHERE pnrnumber=($4)' ;
    var updateValues = [req.body.trainid,req.body.seats,req.body.userid,bookingPnr];
    connection.query(query,updateValues,(err,result) => {
        if(err) {
            logger.error('Error in Update Booking List' + err);
            res.status(500).send('Server error!' + err);
        } 
        res.status(200).json({ status: 'Updated Successfully' });
    })

}