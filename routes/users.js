const express = require('express');
//const path = require('path');
//const rootDir = require('../util/path');
const router = express.Router();
const userCtrl = require('../controllers/users');
const authorize = require('../util/authenticate');

router.post('/login',userCtrl.login);

router.post('/register',userCtrl.register);

router.post('/searchTrain',authorize,userCtrl.searchTrain);

router.post('/bookTicket',authorize,userCtrl.bookTicket);


exports.routes = router;