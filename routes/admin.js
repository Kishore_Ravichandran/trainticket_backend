const express = require('express');
//const path = require('path');
//const rootDir = require('../util/path');
const router = express.Router();
const adminCtrl = require('../controllers/admin');
const authorize = require('../util/authenticate');

router.get('/booking',authorize,adminCtrl.booking);
router.get('/trainList',authorize,adminCtrl.trainList);
router.post('/updateBooking',authorize,adminCtrl.updateBooking);


exports.routes = router;