const winston = require('winston');
const winstonDailyRotateFile = require('winston-daily-rotate-file');

const logFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf(
        info => `${info.timestamp} ${info.level}: ${info.message}`,
    )
)

const logger = winston.loggers.add('customLogger',{
    format : logFormat,
    transports : [
        new winstonDailyRotateFile({
            filename : './logs/custom-%DATE%.log',
            datePattern : 'YYYY-DD-MM',
            level : 'info'
        }),
        new winston.transports.Console({
            level : 'info'
        })
    ]
});



module.exports = logger;